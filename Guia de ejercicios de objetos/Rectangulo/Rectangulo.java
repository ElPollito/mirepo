
package rectangulo;

import java.awt.Point;

public class Rectangulo {
    
    Point supIzq;
    Point supDer;
    Point infDer;
    Point infIzq;
    
   
    public Rectangulo(Point p1, Point p2, Point p3, Point p4){
        
        this.supIzq = p1;
        this.supDer = p2;
        this.infDer = p3;
        this.infIzq = p4; 
    }
    public Rectangulo(int base, int altura){
        
        this.infIzq = new Point(0,0);           
        this.supIzq = new Point(0,altura);
        this.supDer = new Point(base,altura);
        this.infDer = new Point(base,0);
    
    }
    
    public int calcularSuperficie(){
    
        int base = supDer.x - supIzq.x;
        int altura = supDer.y - infDer.y;
         
        int superficie = base * altura;
        
        return superficie;
    }
    
    public void desplazar(int x, int y){
       
       
        this.infDer.x += x;
        this.infIzq.x += x;
        this.supDer.x += x;
        this.supIzq.x += x;
        
        this.infDer.y += y;
        this.infIzq.y += y;
        this.supDer.y += y;
        this.supIzq.y += y;
    
    }

    
}
