
package cafeteria;


public class Cafetera {
    
    // Los 2 atributos de la cafetera.
     private int _capacidadMaxima;
     private int _cantidadActual;
    
    // CONSTRUCTOR
    public Cafetera(/*VACIO*/){    // CONSTRUCTOR PREDETERMINADO
        this._capacidadMaxima = 1000;
        this._cantidadActual = 0;
    }
    
    public Cafetera(int _capacidadMaxima){
        this._capacidadMaxima = _capacidadMaxima;
        this._cantidadActual = _capacidadMaxima;
    }
    
    public Cafetera(int capacidadMaxima, int cantidadActual){
    
        _capacidadMaxima = capacidadMaxima;
             
        if(cantidadActual > capacidadMaxima)
            _cantidadActual = capacidadMaxima; 
        else
             this._cantidadActual = cantidadActual;
             
    }
    
    public void llenarCafetera(){
        this.setCantidadActual(getCapacidadMaxima());
    }
    
    public void servirTaza(int cantDeseada){
        if (getCantidadActual() >= cantDeseada)
            setCantidadActual(getCantidadActual() - cantDeseada);                       
        else
            setCantidadActual(0);
       
    }
    public void vaciarCafetera(){
        this.setCantidadActual(0);
    }
    public void agregarCafe(int cantidadIndicada){
        this.setCantidadActual(this.getCantidadActual() + cantidadIndicada);
    }

    /**
     * @return the _capacidadMaxima
     */
    public int getCapacidadMaxima() {
        return _capacidadMaxima;
    }

    /**
     * @param _capacidadMaxima the _capacidadMaxima to set
     */
    public void setCapacidadMaxima(int _capacidadMaxima) {
        this._capacidadMaxima = _capacidadMaxima;
    }

    /**
     * @return the _cantidadActual
     */
    public int getCantidadActual() {
        return _cantidadActual;
    }

    /**
     * @param _cantidadActual the _cantidadActual to set
     */
    public void setCantidadActual(int _cantidadActual) {
        this._cantidadActual = _cantidadActual;
    }
   
    
}
