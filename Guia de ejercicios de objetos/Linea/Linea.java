

package linea2;

import java.awt.Point;

public class Linea {
    
    private Point puntoA;              
    private Point puntoB;
    
    
    public Linea(){
        this.puntoA = new Point(); 
        this.puntoB = new Point(); 
    }
    
    public Linea(Point puntoA, Point puntoB){
        this.puntoA = puntoA;
        this.puntoB = puntoB;  
    }
    
    public void mueveDerecha(int distancia){
     
        this.puntoA.x += distancia; // this.puntoA.x = this.puntoA.x + distancia;
        this.puntoB.x += distancia; // this.puntoB.x = this.puntoB.x + distancia;
        
    }
    
       public void mueveIzquierda(int distancia){
     
        this.puntoA.x -= distancia;
        this.puntoB.x -= distancia;
        
    }
       
        public void mueveArriba(int distancia){
     
        this.puntoA.y+= distancia;
        this.puntoB.y += distancia;
        
    }
        
        public void mueveAbajo(int distancia){
     
        this.puntoA.y-= distancia;
        this.puntoB.y -= distancia;
        
    }
        
        //getter puntoA
        public Point getPuntoA(){
            return this.puntoA;
        }
        
        public void setPuntoA(Point puntoA){
            this.puntoA = puntoA;
        }
        
        public Point getPuntoB(){
            return this.puntoB;
        }
        
        public void setPuntoB(Point puntoB){
            this.puntoB = puntoB;
        }
        
        public void mostrarPuntos()
        {
            System.out.println("[("+this.puntoA.x+", "+this.puntoA.y+"), ("+this.puntoB.x+", "+this.puntoB.y+")]");
        }
        
        
        
}
