/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juegoderol;

/**
 *
 * @author Pablo
 */
public abstract class Personaje {
    
    int vidaActual;
    int vidaTotal;
    boolean paralizado = false;
    
    Juego juego;
    
    
    public Personaje(Juego juego){
        this.juego = juego;
    }
    
    
    public abstract void atacar(Personaje enemigo);
    
    
}
