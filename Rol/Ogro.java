/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juegoderol;

/**
 *
 * @author Pablo
 */
public class Ogro extends Personaje{
    
    public Ogro(Juego juego){
        super(juego);
        this.vidaActual = 100;
        this.vidaTotal = 100;
    }
    
    @Override
    public void atacar(Personaje enemigo) {   
        int golpe = 15 + (int)(Math.random()*(25 -15));
        enemigo.vidaActual -= golpe;
        
        juego.loggear("El ogro golpea con su garrote...");
        juego.loggear("El personaje recibe "+golpe+" ptos. de daño");      
    }
    
}
