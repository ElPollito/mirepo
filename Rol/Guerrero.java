/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juegoderol;

/**
 *
 * @author Pablo
 */
public class Guerrero extends Personaje implements Grosso {
    
    public Guerrero(Juego juego){
        super(juego);
        this.vidaActual = 100;
        this.vidaTotal = 100;
    }
    

    @Override
    public void atacar(Personaje enemigo) {       
        enemigo.vidaActual -= 20;
        juego.loggear("El guerrero golpea con una estocada...");
        juego.loggear("El ogro recibe 20 ptos. de daño");
    }

    @Override
    public void otroAtaque(Personaje enemigo) {
        juego.loggear("El realiza un ataque desprotegido, provocandose 10 ptos. de daño a si mismo...");
        juego.loggear("Golpea! El ogro recibe 30 ptos. de daño");
        
        enemigo.vidaActual -= 30;
        this.vidaActual -= 10;
    }
    
    
    
    
}
