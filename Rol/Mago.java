/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juegoderol;

/**
 *
 * @author Pablo
 */
public class Mago extends Personaje implements Grosso {
    
    
    
    public Mago(Juego juego){
        super(juego);
        this.vidaActual = 60;
        this.vidaTotal = 60;
    }
    

    @Override
    public void atacar(Personaje enemigo) {   
        enemigo.vidaActual -= 30;
        
        juego.loggear("El mago intenta atacar con Escarcha helada...");
        juego.loggear("Golpea! El ogro recibe 30 ptos de daño");
        
    }

    @Override
    public void otroAtaque(Personaje enemigo) {
        
        enemigo.vidaActual -= 20;
        
        juego.loggear("El mago intenta con el hechizo Descarga electrica...");
        juego.loggear("El ogro recibe 20 ptos de daño");
        int dado = (int)(Math.random()*100); //random entre 0 y 100
        
        if (dado <= 30){ // Si entro, es porq entre en el 30%
            enemigo.paralizado = true;
            juego.loggear("Funciona! El ogro quedo paralizado!");
        }
        else 
            juego.loggear("El ogro evito la paralisis...");

    
    }
    
    
    
    
}
