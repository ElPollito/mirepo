/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package juegoderol;

/**
 *
 * @author Pablo
 */
public class Arquero extends Personaje implements Grosso {
    
    public Arquero(Juego juego){
        super(juego);
        this.vidaActual = 80;
        this.vidaTotal = 80;
    }
    
    @Override
    public void atacar(Personaje enemigo) {   
        
        int golpe = 10 + (int)(Math.random()*(30 - 10));
        juego.loggear("El arquero intenta atacar con un disparo certero...");
        juego.loggear("Golpea! El ogro recibe "+golpe+" ptos. de daño");
        
        enemigo.vidaActual -= golpe;
    }

    @Override
    public void otroAtaque(Personaje enemigo) {
        
        
        int dado = (int)(Math.random()*100); //random entre 0 y 100
        
        if (dado <= 30){ // Si entro, es porq entre en el 30%
            juego.loggear("El arquero ataca con un disparo localizado...");
            juego.loggear("Critico!! El ogro recibe 40 ptos. de daño");
            enemigo.vidaActual -= 40;
        }
        else{
            juego.loggear("Fallo!! El ogro no recibe daño");
        }
    
    }
    
    
    
    
}
