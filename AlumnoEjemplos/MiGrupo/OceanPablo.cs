using System;
using System.Collections.Generic;
using System.Text;
using TgcViewer.Example;
using TgcViewer;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using Microsoft.DirectX;
using TgcViewer.Utils.Modifiers;
using TgcViewer.Utils.Terrain;
using TgcViewer.Utils.Shaders;
using TgcViewer.Utils.TgcSceneLoader;

namespace Examples.Outdoor
{

    /// <summary>
    /// Ejemplo EjemploSimpleTerrain:
    /// Unidades Involucradas:
    ///     # Unidad 7 - T�cnicas de Optimizaci�n - Heightmap
	///
    /// Muestra como crear un terreno en base a una textura de Heightmap y
    /// le aplica arriba una textura de color (DiffuseMap).
    /// Se utiliza la herramienta TgcSimpleTerrain
    /// Posee modifiers para variar la textura de Heightmap, el DiffuseMap y el vector de escala del terreno.
    /// 
    /// Autor: Mat�as Leone, Leandro Barbagallo
    /// 
    /// </summary>
    public class EjemploSimpleTerrain : TgcExample
    {

        TgcSimpleTerrain terrain;
        TgcSkyBox skyBox;
        
        string currentHeightmap;
        string currentTexture;
        float currentScaleXZ;
        float currentScaleY;
        Effect effect;
        float time;
        TgcMesh mesh;

        public override string getCategory()
        {
            return "Pablo";
        }

        public override string getName()
        {
            return "Ocean";
        }

        public override string getDescription()
        {
            return "Crea un oceano en base a un HeightMap";
        }

        public override void init()
        {
            Device d3dDevice = GuiController.Instance.D3dDevice;

            //Path de Heightmap default del terreno y Modifier para cambiarla
            currentHeightmap = GuiController.Instance.AlumnoEjemplosMediaDir + "Heighmaps\\" + "Ocean.jpg";
           
            //Modifiers para variar escala del mapa
            currentScaleXZ = 20f;
          
            currentScaleY = 1.3f;

            //Path de Textura default del terreno y Modifier para cambiarla
            currentTexture = GuiController.Instance.AlumnoEjemplosMediaDir + "Heighmaps\\" + "water512.jpg";
           

            //Cargar terreno: cargar heightmap y textura de color
            terrain = new TgcSimpleTerrain();
            terrain.loadHeightmap(currentHeightmap, currentScaleXZ, currentScaleY, new Vector3(0,0,0));
            terrain.loadTexture(currentTexture);

            // Crear SkyBox:
            skyBox = new TgcSkyBox();
            skyBox.Center = new Vector3(0, 0, 0);
            skyBox.Size = new Vector3(8000, 8000, 8000);
            string texturesPath = GuiController.Instance.AlumnoEjemplosMediaDir + "Texturas\\SkyBox1\\";
            skyBox.setFaceTexture(TgcSkyBox.SkyFaces.Up, texturesPath + "phobos_up.jpg");
            skyBox.setFaceTexture(TgcSkyBox.SkyFaces.Down, texturesPath + "phobos_dn.jpg");
            skyBox.setFaceTexture(TgcSkyBox.SkyFaces.Left, texturesPath + "phobos_lf.jpg");
            skyBox.setFaceTexture(TgcSkyBox.SkyFaces.Right, texturesPath + "phobos_rt.jpg");
            skyBox.setFaceTexture(TgcSkyBox.SkyFaces.Front, texturesPath + "phobos_bk.jpg");
            skyBox.setFaceTexture(TgcSkyBox.SkyFaces.Back, texturesPath + "phobos_ft.jpg");
            skyBox.SkyEpsilon = 50f;
            skyBox.updateValues();


            //Configurar FPS Camara
            GuiController.Instance.FpsCamera.Enable = true;
            GuiController.Instance.FpsCamera.MovementSpeed = 100f;
            GuiController.Instance.FpsCamera.JumpSpeed = 100f;
            GuiController.Instance.FpsCamera.setCamera(new Vector3(-722.6171f, 495.0046f, -31.2611f), new Vector3(164.9481f, 35.3185f, -61.5394f));

            //Cargar Shader personalizado
            effect = TgcShaders.loadEffect(GuiController.Instance.AlumnoEjemplosDir + "Shaders\\Ocean.fx");

            // le asigno el efecto a la malla 
            terrain.Effect = effect;

            // indico que tecnica voy a usar 
            // Hay effectos que estan organizados con mas de una tecnica.
            terrain.Technique = "RenderScene";

            time = 0;

            //Crear loader
            TgcSceneLoader loader = new TgcSceneLoader();


            //Cargar mesh
            TgcScene scene = loader.loadSceneFromFile(GuiController.Instance.AlumnoEjemplosMediaDir + "Modelos\\Olla\\Olla-TgcScene.xml");
            mesh = scene.Meshes[0].createMeshInstance("olla",new Vector3(1,1,1),new Vector3(0,0,0),new Vector3(0.1f,0.1f,0.1f));
            
           // mesh.move(new Vector3(30,0, 30));

        }

        public override void render(float elapsedTime)
        {
            Device device = GuiController.Instance.D3dDevice;
            time += elapsedTime;


            // Cargar variables de shader, por ejemplo el tiempo transcurrido.
            effect.SetValue("time", time);

            // dibujo la malla pp dicha
            terrain.render();
            skyBox.render();

           
           

            float posY = 3*(float)Math.Cos(time) + 3*(float)Math.Cos(time);
            //float posY = 3 * (float)Math.Cos(time);

            mesh.move(new Vector3(0,((posY)-mesh.Position.Y), 0));

           
            mesh.render();


        }

        public override void close()
        {
            terrain.dispose();
        }

    }
}
