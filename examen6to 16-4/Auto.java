/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package examen6to;

/**
 *
 * @author Pablo
 */
public class Auto {
    
    double cantNaftaActual;
    double cantNaftaMaxima;
    
    
    public Auto(){
        this.cantNaftaActual = 0;
        this.cantNaftaMaxima = 50;
    }
    public Auto(double act, double max){
        this.cantNaftaActual = act;
        this.cantNaftaMaxima = max;
    }
    
    public void llenarTanque(){
        this.cantNaftaActual = this.cantNaftaMaxima;
    }
    
    // 10kms ---- 1lt
    //  50    -----  x lt
    public void salirAPasear(int kms){
        this.cantNaftaActual =- kms * 0.1;
        this.cantNaftaActual = this.cantNaftaActual - kms * 0.1; 
    }
    
    public static void main(String[] args) {
        
        Auto auto = new Auto(35,45);
        auto.salirAPasear(50);
        auto.llenarTanque();
    }
    
}
