/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen6to;

import java.awt.Point;

public class Circunferencia {

    Point centro;
    int radio;
    
    public Circunferencia(Point centro, int radio) {
        this.centro = centro;
        this.radio = radio;
    }
    
    public Circunferencia(Point izq, Point der, Point sup, Point inf){  
        this.centro = new Point(inf.x,der.y);
        this.radio = der.x - centro.x;
     } 
    
    public double calcularSuperficie(){
        return Math.PI * (radio * radio);
   
    }
    
    public void desplazar(int x , int y){
        this.centro.x += x; 
        this.centro.y += y; 
    }
    
    public static void main(String[] args) {
        
        Circunferencia c1 = new Circunferencia(new Point(2,4),4);
        
    }
}
